# Copyright 2021  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python senate_2016_to_blt.py <filename without extension, e.g. aec-senate-formalpreferences-20499-ACT>

import csv
import io
import itertools
import sys
import zipfile

def aatoi(aa):
	i = 0
	for letter in aa:
		i += ord(letter) - ord('A') + 1
		i *= 26
	i //= 26
	return i

def itoaa(i):
	aa = ''
	while i > 0:
		aa = chr(ord('A') + (i - 1) % 26) + aa
		i = (i - 1) // 26
	return aa

with zipfile.ZipFile('aec-senate-candidateinformation-' + sys.argv[1].split('-')[3] + '.zip', 'r') as zipf:
	with zipf.open('aec-senate-candidateinformation-' + sys.argv[1].split('-')[3] + '.csv', 'r') as f:
		reader = csv.DictReader(io.TextIOWrapper(f, newline=''))
		
		# Read candidates
		candidates = []
		groups = []
		
		for row in reader:
			if row['state_ab'] != sys.argv[1].split('-')[4]:
				continue
			if row['div_nm'] != '':
				# Get only Senate candidates
				continue
			
			if row['ticket'] != 'UG':
				if len(groups) < aatoi(row['ticket']):
					groups.append([])
				groups[-1].append(len(candidates))
			
			candidates.append(row['surname'] + ' ' + row['ballot_given_nm'])
		
		# Print BLT header
		print('{} 6'.format(len(candidates)))

with zipfile.ZipFile(sys.argv[1] + '.zip', 'r') as zipf:
	with zipf.open(sys.argv[1] + '.csv', 'r') as f:
		reader = csv.DictReader(io.TextIOWrapper(f, newline=''))
		
		next(reader) # Skip row of '-----'s
		
		# Read ballots
		ballots = []
		
		for line in reader:
			ballot = line['Preferences'].split(',')
			ballot = ['1' if x == '*' or x == '/' else x for x in ballot] # Replace * with 1
			
			ballot_atl = ballot[:len(groups)]
			ballot_btl = ballot[len(groups):]
			
			prefs_atl = []
			for i in range(1, len(groups) + 1):
				i = str(i)
				if sum(1 for x in ballot_atl if x == i) == 1:
					# Valid in sequence
					prefs_atl.append(ballot_atl.index(i))
				else:
					break
			
			prefs_btl = []
			for i in range(1, len(candidates) + 1):
				i = str(i)
				if sum(1 for x in ballot_btl if x == i) == 1:
					# Valid in sequence
					prefs_btl.append(ballot_btl.index(i))
				else:
					break
			
			if len(prefs_btl) >= 6:
				# Valid BTL
				ballots.append(prefs_btl)
			elif len(prefs_atl) >= 1:
				# Valid ATL
				prefs = []
				for group in prefs_atl:
					prefs.extend(groups[group])
				ballots.append(prefs)
			
			if len(ballots) % 10000 == 0:
				print(len(ballots), file=sys.stderr)

ballots.sort()

# Print the ballots

for k, g in itertools.groupby(ballots):
	print('{} {} 0'.format(len(list(g)), ' '.join(str(x + 1) for x in k)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate))

print('"{}"'.format(sys.argv[1]))
