# Copyright 2021–2022  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python senate_2007_to_blt.py <BTL preferences filename without extension, e.g. SenateStateBtlDownload-17875-WA>

import csv
import io
import itertools
import sys
import zipfile

def aatoi(aa):
	i = 0
	for letter in aa:
		i += ord(letter) - ord('A') + 1
		i *= 26
	i //= 26
	return i

def itoaa(i):
	aa = ''
	while i > 0:
		aa = chr(ord('A') + (i - 1) % 26) + aa
		i = (i - 1) // 26
	return aa

print('# Source: Converted by RunasSudo from files named SenateGroupVotingTicketsDownload-{0}.csv, SenateUseOfGvtByGroupDownload-{0}.csv and {1}.zip at https://results.aec.gov.au/{0}/Website/SenateDownloadsMenu-{0}-csv.htm, using senate_2007_to_blt.py'.format(sys.argv[1].split('-')[1], sys.argv[1]))

with open('SenateGroupVotingTicketsDownload-' + sys.argv[1].split('-')[1] + '.csv', 'r', newline='') as f:
	next(f) # Skip info row
	
	reader = csv.DictReader(f)
	
	# Read candidates from first GVT
	candidates = []
	
	for row in reader:
		if row['State'] != sys.argv[1].split('-')[2]:
			continue
		if row['OwnerTicket'] != 'A' or row['TicketNo'] != '1':
			# End of first ticket
			break
		
		candidates.append((row['CandidateID'], row['Surname'] + ' ' + row['GivenNm']))
	
	# Print BLT header
	print('{} 6'.format(len(candidates)))

with open('SenateGroupVotingTicketsDownload-' + sys.argv[1].split('-')[1] + '.csv', 'r', newline='') as f:
	next(f) # Skip info row
	
	reader = csv.DictReader(f)
	
	# Read GVTs
	gvts = {}
	
	for row in reader:
		if row['State'] != sys.argv[1].split('-')[2]:
			continue
		if row['OwnerTicket'] not in gvts:
			gvts[row['OwnerTicket']] = {}
		if row['TicketNo'] not in gvts[row['OwnerTicket']]:
			gvts[row['OwnerTicket']][row['TicketNo']] = []
		
		gvts[row['OwnerTicket']][row['TicketNo']].append((next(i for i, c in enumerate(candidates) if c[0] == row['CandidateID']), int(row['PreferenceNo'])))
	
	for _, tickets in gvts.items():
		for _, ticket in tickets.items():
			ticket[:] = [x[0] for x in sorted(ticket, key=lambda y: y[1])]

with open('SenateUseOfGvtByGroupDownload-' + sys.argv[1].split('-')[1] + '.csv', 'r', newline='') as f:
	next(f) # Skip info row
	
	reader = csv.DictReader(f)
	
	# Read ATL votes
	atl_votes = []
	
	for row in reader:
		if row['StateAb'] != sys.argv[1].split('-')[2]:
			continue
		if row['Ticket'] == 'UG':
			continue
		if row['TicketVotes'] == '0':
			continue
		atl_votes.append((row['Ticket'], int(row['TicketVotes'])))

# Print ATL votes

print('# ATL votes')

for gvt in atl_votes:
	tickets = gvts[gvt[0]]
	for _, ticket in tickets.items():
		if gvt[1] % len(tickets) == 0:
			print('{} {} 0'.format(gvt[1] // len(tickets), ' '.join(str(x + 1) for x in ticket)))
		else:
			# Manual adjustment required to round to whole numbers according to AEC drawing of lots
			print('{} {} 0'.format(gvt[1] / len(tickets), ' '.join(str(x + 1) for x in ticket)))

with zipfile.ZipFile(sys.argv[1] + '.zip', 'r') as zipf:
	with zipf.open('SenateStateBTLPreferences-' + sys.argv[1].split('-')[1] + '-' + sys.argv[1].split('-')[2] + '.txt', 'r') as f:
		f = io.TextIOWrapper(f, newline='')
		
		next(f) # Skip info row
		
		reader = csv.DictReader(f, dialect='excel-tab')
		
		# Read BTL votes
		ballots = []
		
		last_ballot = None
		for row in reader:
			if last_ballot is None or (row['Batch'], row['Paper']) != last_ballot:
				last_ballot = (row['Batch'], row['Paper'])
				ballots.append([])
				
				if len(ballots) % 1000 == 0:
					print(len(ballots), file=sys.stderr)
			
			ballot = ballots[-1]
			
			row['Preference'] = row['Preference'].strip().strip('.').strip('\\').strip(']')
			if not row['Preference'] or '?' in row['Preference']:
				continue
			if row['Preference'] in ['-', 'Q', '`', 'sv', '03.5', 'A', 'XI', 'noo', 'u', 'M', 'n'] or 'L' in row['Preference']:
				# Manual blacklist
				continue
			if '/' in row['Preference'] or '*' in row['Preference'] or row['Preference'] == 'X':
				row['Preference'] = -1
			else:
				pref = int(row['Preference'])
				if pref != 0:
					ballot.append((next(i for i, c in enumerate(candidates) if c[0] == row['CandidateID']), pref))
		
		for ballot in ballots:
			# If both tick/cross and 1, ignore the tick
			if any(x[1] == -1 for x in ballot):
				if any(x[1] == 1 for x in ballot):
					ballot[:] = [x for x in ballot if x[1] != -1]
				else:
					ballot[:] = [(x[0], 1) if x[1] == -1 else x for x in ballot]
			
			# Consecutive preferences only
			newbal = []
			for i in range(1, len(candidates) + 1):
				prefs = [x for x in ballot if x[1] == i]
				if len(prefs) == 1:
					newbal.append(prefs[0][0])
				else:
					break
			
			ballot[:] = newbal

ballots.sort()

# Print BTL votes

print('# BTL votes')

for k, g in itertools.groupby(ballots):
	print('{} {} 0'.format(len(list(g)), ' '.join(str(x + 1) for x in k)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate[1]))

print('"SenateUseOfGvtByGroupDownload-{} {}"'.format(sys.argv[1].split('-')[1], sys.argv[1]))
