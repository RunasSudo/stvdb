# Copyright 2021  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python cambridge2017_to_blt.py <CSV file>

import csv
import itertools
import sys

candidates = []

with open(sys.argv[1], 'r', newline='') as f:
	reader = csv.reader(f)
	
	# Skip header row
	next(reader)
	
	# Read ballots
	ballots = []
	
	for line in reader:
		ballot = []
		ballots.append(ballot)
		
		for cand in line[1:]:
			if not cand:
				#break
				continue
			if cand == 'overvote':
				#break
				continue
			if cand not in candidates:
				candidates.append(cand)
			if cand not in ballot:
				ballot.append(cand)

candidates.sort()

print('{} 9'.format(len(candidates)))

# Print the ballots

for ballot in ballots:
	print('1 {} 0'.format(' '.join(str(candidates.index(x) + 1) for x in ballot)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate))

print('"' + sys.argv[1].replace(' - Orderings.csv', '') + '"')
