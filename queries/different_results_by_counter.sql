-- Get elections where different counters produced different results (for the same method)
SELECT results.* FROM (
	SELECT * FROM (
		SELECT election, method, COUNT(DISTINCT winners) AS count FROM results WHERE random IS NULL OR random <> 1 GROUP BY election, method
	) WHERE count > 1
) a
JOIN results ON results.election = a.election AND results.method = a.method
