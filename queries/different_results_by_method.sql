-- Get elections where different methods produced different results
SELECT results.* FROM (
--SELECT COUNT(DISTINCT election) FROM (
	SELECT * FROM (
		SELECT election, COUNT(DISTINCT winners) AS count, SUM(COALESCE(random, 0)) AS n_random FROM results GROUP BY election
	) WHERE count > 1 AND n_random = 0
) a
JOIN results ON results.election = a.election
ORDER BY election
