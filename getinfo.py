#!/usr/bin/env python3
import csv
import os
import sqlite3

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()

blt_files = set()

# Walk over elections and add new elections

for dirpath, dirnames, filenames in os.walk('.'):
	if '/.git' in dirpath:
		continue
	
	for filename in filenames:
		if not filename.endswith('.blt'):
			continue
		
		filepath = os.path.join(dirpath, filename)
		print(filepath)
		
		blt_files.add(filepath)
		
		cur.execute('SELECT COUNT(*) FROM elections WHERE path=?', (filepath,))
		if cur.fetchone()[0] > 0:
			# Already exists
			continue
		
		# New election
		with open(filepath, 'r', encoding='iso-8859-1') as f:
			lines = (l for l in f if not l.startswith('#'))
			
			header = next(lines)
			num_candidates = int(header.split()[0])
			num_seats = int(header.split()[1])
			
			ballots = 0
			for line in lines:
				if line.startswith('0'):
					break
				
				value = float(line.split()[0])
				ballots += value
		
		cur.execute('INSERT INTO elections VALUES (?, ?, ?, ?)', (filepath, num_candidates, num_seats, ballots))
		con.commit()

# Delete non-existent elections
cur.execute('SELECT path FROM elections')
for filepath in cur.fetchall():
	if filepath[0] not in blt_files:
		print('Deleting entry for nonexistent election {}'.format(filepath[0]))
		cur.execute('DELETE FROM elections WHERE path=?', (filepath[0],))

cur.execute('SELECT DISTINCT election FROM results')
for filepath in cur.fetchall():
	if filepath[0] not in blt_files:
		print('Deleting results for nonexistent election {}'.format(filepath[0]))
		cur.execute('DELETE FROM results WHERE election=?', (filepath[0],))

con.commit()
con.close()
