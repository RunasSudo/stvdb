# Copyright 2021  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python act_to_blt.py <electorate name>

import csv
import itertools
import sys

with open('Electorates.txt', 'r', newline='') as f:
	reader = csv.DictReader(f)
	
	# Get ecode
	for electorate in reader:
		if electorate['electorate'] == sys.argv[1]:
			ecode = electorate['ecode']
			break
	else:
		raise Exception('Invalid electorate name')

with open('Candidates.txt', 'r', newline='') as f:
	reader = csv.DictReader(f)
	
	# Read candidates
	candidates = []
	pcode_map = {}
	
	for candidate in reader:
		if candidate['ecode'] != ecode:
			continue
		candidates.append(candidate['cname'])
		pcode_map[(candidate['pcode'], candidate['ccode'])] = len(candidates)

print('{} 5'.format(len(candidates)))

with open(sys.argv[1] + 'Total.txt', 'r', newline='') as f:
	reader = csv.DictReader(f)
	
	# Read ballots
	ballots = []
	
	last_pindex = None
	for line in reader:
		if last_pindex is None or line['pindex'] != last_pindex:
			last_pindex = line['pindex']
			ballots.append([])
			
			if len(ballots) % 10000 == 0:
				print(len(ballots), file=sys.stderr)
		
		ballot = ballots[-1]
		ballot.append(pcode_map[(line['pcode'], line['ccode'])])
		
		assert int(line['pref']) == len(ballot)

ballots.sort()

# Print the ballots

for k, g in itertools.groupby(ballots):
	print('{} {} 0'.format(len(list(g)), ' '.join(str(x) for x in k)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate))

print('"' + sys.argv[1] + 'Total"')
