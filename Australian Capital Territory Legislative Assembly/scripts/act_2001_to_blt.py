# Copyright 2021  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python act_2001_to_blt.py <electorate name>

import csv
import itertools
import sys

with open('tblElectorates.txt', 'r', newline='') as f:
	reader = csv.DictReader(f)
	
	# Get ecode
	for electorate in reader:
		if electorate['txtElectorate'] == sys.argv[1]:
			ecode = electorate['ecode']
			break
	else:
		raise Exception('Invalid electorate name')

with open('tblCands.txt', 'r', newline='') as f:
	reader = csv.DictReader(f)
	
	# Read candidates
	candidates = []
	pcode_map = {}
	
	for candidate in reader:
		if candidate['ecode'] != ecode:
			continue
		candidates.append(candidate['cname'])
		pcode_map[(candidate['pcode'], candidate['ccode'])] = len(candidates)

print('{} 5'.format(len(candidates)))

# Read ballots
ballots = []

def ballots_from_file(f):
	reader = csv.DictReader(f)
	
	last_pindex = None
	for line in reader:
		if last_pindex is None or (line['batch'], line['pindex']) != last_pindex:
			last_pindex = (line['batch'], line['pindex'])
			formal = True
			ballots.append([])
			
			if len(ballots) % 10000 == 0:
				print(len(ballots), file=sys.stderr)
		
		ballot = ballots[-1]
		
		if formal:
			if int(line['pref']) == len(ballot) + 1:
				# Correct preference
				ballot.append(pcode_map[(line['pcode'], line['ccode'])])
			elif int(line['pref']) == len(ballot):
				# Duplicate previous preference
				ballot.pop()
				formal = False
			else:
				# Informal
				formal = False

with open('tbl' + sys.argv[1][:4] + 'Paper.txt', 'r', newline='') as f:
	ballots_from_file(f)

with open('tbl' + sys.argv[1][:4] + 'Electronic.txt', 'r', newline='') as f:
	ballots_from_file(f)

ballots.sort()

# Print the ballots

for k, g in itertools.groupby(ballots):
	print('{} {} 0'.format(len(list(g)), ' '.join(str(x) for x in k)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate))

print('"tbl{0}Paper tbl{0}Electronic"'.format(sys.argv[1][:4]))
