import sys

print('15 1')

with open(sys.argv[1], 'r') as f:
	for line in f:
		line = line.strip()
		
		print('1', end='')
		
		groups = line.split(',')
		for group in groups:
			print(' {}'.format('='.join(str(ord(c) - ord('A') + 1) for c in group)), end='')
		
		print(' 0')

print('0')

for c in range(ord('A'), ord('O') + 1):
	print('"Candidate {}"'.format(chr(c)))

print('"2008 Wikimedia Foundation Board of Trustees"')
