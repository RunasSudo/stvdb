# Copyright 2022  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python nswec_lc2011_to_blt.py

from bs4 import BeautifulSoup
import csv
import io
import itertools
import sys
import zipfile

with zipfile.ZipFile('sge_2011_lc_preferences.zip', 'r') as zipf:
	# Read candidates

	#groups = {}
	candidates = [] 
	candidate_id_map = {}
	
	with io.TextIOWrapper(zipf.open('Candidate.csv', 'r'), encoding='utf-8') as f:
		reader = csv.DictReader(f)
		
		for line in reader:
			candidates.append(line['CANDIDATE_NAME'])
			candidate_id_map[line['CANDIDATE_ID']] = len(candidates)
			
			#if line['GROUP_CODE'] not in groups:
			#	groups[line['GROUP_CODE']] = []
			#groups[line['GROUP_CODE']].append(len(candidates))
	
	print('# Source: Converted by RunasSudo from file named sge_2011_lc_preferences.zip at https://pastvtr.elections.nsw.gov.au/SGE2011/lc_prefdata.htm, using nswec_lc2011_to_blt.py')
	print('{} 21'.format(len(candidates)))
	
	# Read ballots
	
	ballots = []
	
	with io.TextIOWrapper(zipf.open('BTLExpandedPreference.csv', 'r'), encoding='utf-8') as f:
		reader = csv.DictReader(f)
		
		last_bpid = None
		for line in reader:
			if last_bpid is None or line['VC_BALLOT_PAPER_ID'] != last_bpid:
				last_bpid = line['VC_BALLOT_PAPER_ID']
				ballots.append([])
				
				if len(ballots) % 10000 == 0:
					print(len(ballots), file=sys.stderr)
			
			ballot = ballots[-1]
			ballot.append((int(line['PREFERENCE']), candidate_id_map[line['CANDIDATE_ID']]))

# Process ballots into full preference lists

preferences = []

for ballot in ballots:
	ballot.sort(key=lambda x: x[0])
	preferences.append([x[1] for x in ballot])

preferences.sort()

# Print the ballots

for k, g in itertools.groupby(preferences):
	print('{} {} 0'.format(len(list(g)), ' '.join(str(x) for x in k)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate))

print('"sge_2011_lc_preferences"')
