# Copyright 2022  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python nswec_lc_to_blt.py <election ID, e.g. SG1901> <election filename prefix, e.g. SGE1901>

from bs4 import BeautifulSoup
import csv
import io
import itertools
import sys
import zipfile

# Read candidates

groups = {}
candidates = []

with open(sys.argv[2] + ' LC Candidates.csv', 'r', newline='') as f:
	reader = csv.DictReader(f)
	
	for line in reader:
		candidates.append(line['Group/Candidates in Ballot Order'])
		
		if line['Group'] not in groups:
			groups[line['Group']] = []
		groups[line['Group']].append(len(candidates))

print('# Source: Converted by RunasSudo from files named "{0} LC Pref Data Statewide.zip" and "{0} LC Candidates.xlsx" at https://pastvtr.elections.nsw.gov.au/{1}/LC/state/preferences, using nswec_lc_to_blt.py'.format(sys.argv[2], sys.argv[1]))
print('{} 21'.format(len(candidates)))

# Read ballots

ballots = []

with zipfile.ZipFile(sys.argv[2] + ' LC Pref Data Statewide.zip', 'r') as zipf:
	with io.TextIOWrapper(zipf.open(sys.argv[2] + ' LC Pref Data_NA_State.txt', 'r'), encoding='utf-8') as f:
		reader = csv.DictReader(f, dialect='excel-tab')
		
		last_bpid = None
		for line in reader:
			#if line['Formality'] != 'Formal':
			#	continue
			
			if last_bpid is None or line['VCBallotPaperID'] != last_bpid:
				last_bpid = line['VCBallotPaperID']
				ballots.append([])
				
				if len(ballots) % 10000 == 0:
					print(len(ballots), file=sys.stderr)
			
			if not line['PreferenceNumber']:
				continue
			
			ballot = ballots[-1]
			ballot.append((int(line['PreferenceNumber']), line['CandidateName'], line['GroupCode']))

# Process ballots into full preference lists

preferences = []

for ballot in ballots:
	ballot_prefs = []
	ballot.sort(key=lambda x: x[0])
	
	for pref in ballot:
		if not pref[1]:
			# ATL group
			ballot_prefs.extend(groups[pref[2]])
		else:
			#ballot_prefs.append(pref[1])
			ballot_prefs.append(candidates.index(pref[1]) + 1)
	
	preferences.append(ballot_prefs)

preferences.sort()

# Print the ballots

for k, g in itertools.groupby(preferences):
	print('{} {} 0'.format(len(list(g)), ' '.join(str(x) for x in k)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate))

print('"' + sys.argv[2] + ' LC Pref Data Statewide"')
