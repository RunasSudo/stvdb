# Copyright 2021  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python minneapolis_to_blt.py <CSV file>

import csv
import itertools
import sys

candidates = []

with open(sys.argv[1], 'r', newline='') as f:
	reader = csv.reader(f)
	
	# Skip header row
	next(reader)
	
	# Read ballots
	ballots = []
	
	for line in reader:
		ballot = []
		ballots.append(ballot)
		
		if line[1] != 'undervote' and line[1] != 'overvote':
			if line[1] not in candidates:
				candidates.append(line[1])
			ballot.append(line[1])
		
		if line[2] != 'undervote' and line[2] != 'overvote':
			if line[2] not in candidates:
				candidates.append(line[2])
			if line[2] != line[1]:
				ballot.append(line[2])
		
		if line[3] != 'undervote' and line[3] != 'overvote':
			if line[3] not in candidates:
				candidates.append(line[3])
			if line[3] not in line[1:3]:
				ballot.append(line[3])

candidates.sort()

print('{} 3'.format(len(candidates)))

ballots.sort()

# Print the ballots

for k, g in itertools.groupby(ballots):
	print('{} {} 0'.format(len(list(g)), ' '.join(str(candidates.index(x) + 1) for x in k)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate))

print('"' + sys.argv[1].replace('.csv', '') + '"')
