#!/usr/bin/env python3
import csv
import io
import os
import shutil
import sqlite3
import subprocess
import sys

METHOD = "PRSA 1977"
COUNTER = "prsa_count.nl 4a166fe"

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()

for dirpath, dirnames, filenames in os.walk('.'):
	if '/.git' in dirpath:
		continue
	
	for filename in filenames:
		if not filename.endswith('.blt') or filename == 'tmp.blt':
			continue
		
		filepath = os.path.join(dirpath, filename)
		print(filepath)
		
		cur.execute('SELECT COUNT(*) FROM results WHERE election=? AND method=? AND counter=?', (filepath, METHOD, COUNTER))
		if cur.fetchone()[0] > 0:
			# Already exists
			continue
		
		cur.execute('SELECT candidates, votes FROM elections WHERE path=?', (filepath,))
		candidates, ballots = cur.fetchone()
		
		if candidates > 50:
			# Too many candidates
			continue
		
		compatible = True
		
		if ballots <= 800:
			# Count!
			prolog_inp = ''
			
			with open(filepath, 'r') as fin:
				def ppline(line):
					if '#' in line:
						line = line[:line.index('#')]
					return line.strip()
				
				lines = (ppline(l) for l in fin if not l.startswith('#'))
				
				# Read header
				x = next(lines).split()
				num_cands, num_seats = int(x[0]), int(x[1])
				
				# Read ballots
				ballots = []
				for line in lines:
					if line == '0':
						break
					
					if '=' in line:
						compatible = False
						break
					
					x = line.split()
					mult = int(x[0])
					ballot = [int(p) for p in x[1:] if p != '0']
					ballots.extend([ballot * mult])
		else:
			# Too many votes for prsa_count!
			continue
		
		if not compatible:
			continue
		
		# -------------
		# Output Prolog
		
		def fmt_ballot(ballot):
			return '[{}]'.format(', '.join("'{}'".format(p) for p in ballot))
		
		# Candidates row
		prolog_inp += '[{}].\n'.format(', '.join("'{}'".format(i + 1) for i in range(num_cands)))
		
		# Number of seats
		prolog_inp += '{}.\n'.format(num_seats)
		
		# Ballots
		# prsa_count.nl computes quota incorrectly if a fully informal ballot is included
		prolog_inp += '[{}].\n'.format(', '.join(fmt_ballot(b) for b in ballots if b))
		
		#print(prolog_inp)
		
		# Count using prsa_count
		proc = subprocess.run(['bash', 'prsa_count'], input=prolog_inp, capture_output=True, encoding='utf-8')
		
		if proc.returncode != 0:
			print(' * Failed with code {}'.format(proc.returncode))
			continue
		
		# Parse winners
		winners = []
		lines = iter(proc.stdout.split('\n'))
		for line in lines:
			if line.strip() == 'Final result: the following candidates are elected:':
				break
		else:
			print('No results?!')
			sys.exit(1)
		winners = list(l.strip() for l in lines if l.strip())
		winners.sort(key=lambda x: int(x))
		
		#print('INSERT INTO results VALUES (?, NULL, "PRSA 1977", "prsa_count.nl 4a166fe", NULL, ?, ?)', (filepath, ','.join(str(x) for x in winners), proc.stdout))
		cur.execute('INSERT INTO results VALUES (?, NULL, ?, ?, NULL, ?, ?)', (filepath, METHOD, COUNTER, ','.join(str(x) for x in winners), proc.stdout))
		con.commit()
		
		#sys.exit(0)

con.close()
