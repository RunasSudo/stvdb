#!/usr/bin/env python3
import csv
import io
import os
import re
import shutil
import sqlite3
import subprocess
import sys
import time

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()

for dirpath, dirnames, filenames in os.walk('.'):
	if '/.git' in dirpath:
		continue
	
	for filename in filenames:
		if not filename.endswith('.blt') or filename == 'tmp.blt':
			continue
		
		filepath = os.path.join(dirpath, filename)
		print(filepath)
		
		cur.execute('SELECT COUNT(*) FROM results WHERE election=? AND method="MeekNZ" AND counter="nzmeek 6.7.7"', (filepath,))
		if cur.fetchone()[0] > 0:
			# Already exists
			continue
		
		cur.execute('SELECT votes FROM elections WHERE path=?', (filepath,))
		ballots = cur.fetchone()[0]
		
		compatible = True
		
		if ballots <= 10000:
			# Count!
			with open(filepath, 'r') as fin:
				with open('votes.dat', 'w') as fout:
					for line in fin:
						if '=' in line:
							compatible = False
							break
						if '#' in line:
							line = line[:line.index('#')]
						line = line.replace('" ', '"\n')
						if line.strip():
							print(line.strip(), file=fout)
		else:
			# Sample required - TODO
			continue
		
		if not compatible:
			continue
		
		# Count using nzmeek
		proc = subprocess.run(['./nzmeek'], input='\n\n\n', capture_output=True, encoding='iso-8859-1', timeout=30, check=True)
		
		# Parse winners
		winners = []
		with open('votes.rlt', encoding='iso-8859-1') as f:
			next(f) # Skip header rows
			next(f)
			next(f)
			
			for line in f:
				if line.strip() == '0 0 0':
					break
				bits = line.strip().split()
				if int(bits[1]) > 0:
					winners.append(int(bits[0]))
		
		with open('votes.rlt', encoding='iso-8859-1') as f:
			data = f.read()
		data = data[:-2] # Strip broken EOF indicator
		
		#print(filepath, ','.join(str(x) for x in winners), data)
		cur.execute('INSERT INTO results VALUES (?, NULL, "MeekNZ", "nzmeek 6.7.7", ?, ?)', (filepath, ','.join(str(x) for x in winners), data))
		con.commit()
		
		#sys.exit(0)

con.close()
