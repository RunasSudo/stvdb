#!/usr/bin/env python3
import csv
import io
import os
import sqlite3
import subprocess
import sys

METHOD = "PRSA 1977"
COUNTER = "OpenTally {}".format(subprocess.run(['git', 'describe', '--always', '--dirty=-dev'], cwd='opentally', capture_output=True, encoding='utf-8', check=True).stdout.strip())

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()

for dirpath, dirnames, filenames in os.walk('.'):
	if '/.git' in dirpath:
		continue
	
	for filename in filenames:
		if not filename.endswith('.blt') or filename == 'tmp.blt':
			continue
		
		filepath = os.path.join(dirpath, filename)
		print(filepath)
		
		cur.execute('SELECT COUNT(*) FROM results WHERE election=? AND method=? AND counter LIKE "OpenTally %"', (filepath, METHOD,))
		if cur.fetchone()[0] > 0:
			# Already exists
			continue
		
		# Temp: Filter by prsa_count results
		cur.execute('SELECT COUNT(*) FROM results WHERE election=? AND method="PRSA 1977" AND counter LIKE "prsa_count.nl %"', (filepath,))
		if cur.fetchone()[0] == 0:
			continue
		
		cur.execute('SELECT votes FROM elections WHERE path=?', (filepath,))
		ballots = cur.fetchone()[0]
		
		if ballots <= 10000:
			# Count!
			pass
		else:
			# Sample required - TODO
			continue
		
		# Count using OpenTally
		proc = subprocess.run(['opentally/target/release/opentally', 'stv', filepath, '--output', 'csv', '--numbers', 'fixed', '--decimals', '6', '--round-surplus-fractions', '3', '--round-values', '3', '--round-votes', '3', '--round-quota', '3', '--quota-criterion', 'geq', '--ties', 'backwards', 'random', '--random-seed', '0', '--surplus', 'eg', '--surplus-order', 'by_order', '--transferable-only', '--exclusion', 'parcels_by_order', '--no-early-bulk-elect', '--defer-surpluses', '--pp-decimals', '3'], capture_output=True, encoding='utf-8', check=True)
		
		# Parse winners
		winners = []
		reader = csv.reader(io.StringIO(proc.stdout))
		for _ in range(11):
			next(reader) # Skip header rows
		
		for i, row in enumerate(reader):
			if row[-1] == 'Elected':
				winners.append(i + 1)
		
		#print('INSERT INTO results VALUES (?, NULL, ?, ?, NULL, ?, ?)', (filepath, METHOD, COUNTER, ','.join(str(x) for x in winners), proc.stdout))
		cur.execute('INSERT INTO results VALUES (?, NULL, ?, ?, NULL, ?, ?)', (filepath, METHOD, COUNTER, ','.join(str(x) for x in winners), proc.stdout))
		con.commit()
		
		#sys.exit(0)

con.close()
