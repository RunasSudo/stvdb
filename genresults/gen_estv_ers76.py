#!/usr/bin/env python3
import csv
import os
import re
import shutil
import sqlite3
import subprocess
import sys
import time

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()

for dirpath, dirnames, filenames in os.walk('.'):
	if '/.git' in dirpath:
		continue
	
	for filename in filenames:
		if not filename.endswith('.blt') or filename == 'tmp.blt':
			continue
		
		filepath = os.path.join(dirpath, filename)
		print(filepath)
		
		cur.execute('SELECT COUNT(*) FROM results WHERE election=? AND method="ERS76" AND counter="eSTV 1.47"', (filepath,))
		if cur.fetchone()[0] > 0:
			# Already exists
			continue
		
		cur.execute('SELECT votes FROM elections WHERE path=?', (filepath,))
		ballots = cur.fetchone()[0]
		
		compatible = True
		
		if ballots <= 10000:
			# Count!
			with open(filepath, 'r') as fin:
				with open('tmp.dat', 'w') as fout:
					for line in fin:
						if '=' in line:
							compatible = False
							break
						if '#' in line:
							line = line[:line.index('#')]
						line = line.replace('" ', '"\n')
						print(line.strip(), file=fout)
		else:
			# Sample required - TODO
			continue
		
		if not compatible:
			continue
		
		if os.path.exists('tmp.csv'):
			os.remove('tmp.csv')
		
		# Count using eSTV
		proc = subprocess.Popen(['wine', 'estv.exe', 'tmp.dat', '/run', '/ers76', '/psr', '/seed0'])
		
		# eSTV sticks around for a while, so kill it manually once done
		while not os.path.exists('tmp.csv'):
			time.sleep(0.5)
		time.sleep(0.5)
		proc.kill()
		
		# Parse winners
		winners = []
		with open('tmp.csv', 'r', newline='') as f:
			reader = csv.reader(f)
			for _ in range(11):
				next(reader) # Skip header rows
			
			for i, row in enumerate(reader):
				if row[-1] == 'Elected':
					winners.append(i + 1)
		
		with open('tmp.csv', 'r') as f:
			data = f.read()
		data = re.sub(r'eSTV Reg\. .....', 'eSTV Reg. 00000', data)
		
		cur.execute('INSERT INTO results VALUES (?, NULL, "ERS76", "eSTV 1.47", ?, ?)', (filepath, ','.join(str(x) for x in winners), data))
		con.commit()
		
		#sys.exit(0)

con.close()
