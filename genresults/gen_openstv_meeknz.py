#!/usr/bin/env python3
import csv
import io
import os
import re
import shutil
import sqlite3
import subprocess
import sys
import time

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()

for dirpath, dirnames, filenames in os.walk('.'):
	if '/.git' in dirpath:
		continue
	
	for filename in filenames:
		if not filename.endswith('.blt') or filename == 'tmp.blt':
			continue
		
		filepath = os.path.join(dirpath, filename)
		print(filepath)
		
		cur.execute('SELECT COUNT(*) FROM results WHERE election=? AND method="MeekNZ" AND counter="OpenSTV 1.7"', (filepath,))
		if cur.fetchone()[0] > 0:
			# Already exists
			continue
		
		cur.execute('SELECT votes FROM elections WHERE path=?', (filepath,))
		ballots = cur.fetchone()[0]
		
		compatible = True
		
		if ballots <= 10000:
			# Count!
			with open(filepath, 'r') as fin:
				with open('tmp.blt', 'w') as fout:
					for line in fin:
						if '=' in line:
							compatible = False
							break
						if '#' in line:
							line = line[:line.index('#')]
						line = line.replace('" ', '"\n')
						print(line.strip(), file=fout)
		else:
			# Sample required - TODO
			continue
		
		if not compatible:
			continue
		
		# Count using OpenSTV
		proc = subprocess.run(['./openstv.sh', '-r CsvReport', 'MeekNZSTV', 'tmp.blt'], capture_output=True, encoding='utf-8', check=True)
		
		# Parse winners
		winners = []
		reader = csv.reader(io.StringIO(proc.stdout))
		for _ in range(11):
			next(reader) # Skip header rows
		
		for i, row in enumerate(reader):
			if row[-1] == 'Elected':
				winners.append(i + 1)
		
		#print(filepath, ','.join(str(x) for x in winners), proc.stdout)
		cur.execute('INSERT INTO results VALUES (?, NULL, "MeekNZ", "OpenSTV 1.7", ?, ?)', (filepath, ','.join(str(x) for x in winners), proc.stdout))
		con.commit()
		
		#sys.exit(0)

con.close()
