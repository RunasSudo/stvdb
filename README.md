# RunasSudo's STV Database

This is a database of ballot paper preference data from real elections using the single transferable vote (STV) and related preferential voting systems.

Ballot paper preference data is presented in the [BLT file format](https://yingtongli.me/git/OpenTally/tree/docs/blt-fmt.md).

## Database statistics

<table>
<tr><th>Elections</th><td>1,160</td></tr>
<tr><th>Candidates</th><td>15,712</td></tr>
<tr><th>Seats</th><td>4,591</td></tr>
<tr><th>Votes</th><td>107,003,193</td></tr>
</table>

## Scope

Elections are eligible for inclusion if ballot paper preference data is available from ballot papers cast in the election.

The following categories of elections are eligible for inclusion, but are of low priority:

* Elections of private organisations
* Single-winner STV elections, i.e. instant-runoff voting (IRV) elections
* Elections where only sampled, truncated or partial ballot paper preference data is available
* Elections which were counted using nondeterministic STV variants

The following categories of elections are not eligible for inclusion:

* Reconstructed elections where ballot paper data has been simulated based on result sheets
