# Copyright 2022  Lee Yingtong Li (RunasSudo)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage: python nswec_to_blt.py <council name> <number of seats>

from bs4 import BeautifulSoup
import csv
import io
import itertools
import sys
import zipfile

# Read candidates

groups = {}
candidates = []

with open(sys.argv[1] + '-candidates.html', 'r') as f:
	soup = BeautifulSoup(f, 'html.parser')

tbl_cands = soup.find(class_='prcc-data').find(name='table')

cur_group = None
for row in tbl_cands.find(name='tbody').find_all('tr'):
	if 'tr-total' in row.get('class', []) or 'tr-subtotal' in row.get('class', []):
		if row.find('td').find('strong'):
			# Group header
			cur_group = row.find('td').find('strong').text or None
			groups[cur_group] = []
		else:
			# Some other subtotal row
			pass
	else:
		# Candidate
		tds = row.find_all('td')
		if len(tds) == 6:
			# No groups
			cand_name = tds[0].text
			candidates.append(cand_name)
		else:
			# Groups
			cand_name = tds[1].text
			candidates.append(cand_name)
			groups[cur_group].append(candidates.index(cand_name) + 1)

print('# Source: Converted by RunasSudo from file named {}-finalpreferencedatafile.zip at https://pastvtr.elections.nsw.gov.au/LG2101/index, using nswec_to_blt.py'.format(sys.argv[1]))
print('{} {}'.format(len(candidates), sys.argv[2]))

# Read ballots

ballots = []

with zipfile.ZipFile(sys.argv[1] + '-finalpreferencedatafile.zip', 'r') as zipf:
	with io.TextIOWrapper(zipf.open('FinalPreferenceDataFile_Run-00.txt', 'r'), encoding='utf-8') as f:
		reader = csv.DictReader(f, dialect='excel-tab')
		
		last_bpid = None
		for line in reader:
			#if line['Formality'] != 'Formal':
			#	continue
			
			if last_bpid is None or line['VCBallotPaperID'] != last_bpid:
				last_bpid = line['VCBallotPaperID']
				ballots.append([])
				
				if len(ballots) % 10000 == 0:
					print(len(ballots), file=sys.stderr)
			
			if not line['PreferenceNumber']:
				continue
			
			ballot = ballots[-1]
			ballot.append((int(line['PreferenceNumber']), line['CandidateName'], line['GroupCode']))

# Process ballots into full preference lists

preferences = []

for ballot in ballots:
	ballot_prefs = []
	ballot.sort(key=lambda x: x[0])
	
	for pref in ballot:
		if not pref[1]:
			# ATL group
			ballot_prefs.extend(groups[pref[2]])
		else:
			#ballot_prefs.append(pref[1])
			ballot_prefs.append(candidates.index(pref[1]) + 1)
	
	preferences.append(ballot_prefs)

preferences.sort()

# Print the ballots

for k, g in itertools.groupby(preferences):
	print('{} {} 0'.format(len(list(g)), ' '.join(str(x) for x in k)))

print('0')

# Print the candidates

for candidate in candidates:
	print('"{}"'.format(candidate))

print('"' + sys.argv[1] + '-finalpreferencedatafile"')
